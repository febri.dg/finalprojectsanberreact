import React from 'react';
import {
    Platform, View, Text, Image, ScrollView, TextInput, StyleSheet, Button,
    TouchableOpacity, KeyboardAvoidingView
} from 'react-native';

export default class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

  loginHandler() {
    console.log(this.state.userName, ' ', this.state.password)

    if (this.state.password === '12345678') {

      console.log("login bener")
      this.setState({ isError: false });
      this.props.navigation.navigate('Home', {
        userName : this.state.userName
      })

    } else {
      console.log("salah")
      this.setState({ isError: true });
    }

  }

  render() {
    return (
      <KeyboardAvoidingView
          style={styles.container}>
          <ScrollView>
              <View style={styles.containerView}>
                  <Image source={require('./assets/logo.png')} />
                  <Text style={styles.logintext}>Login</Text>
                  <View style={styles.forminput}>
                      <Text style={styles.formtext}>Username</Text>
                      <TextInput
                      style={styles.input}
                      placeholder='Masukkan Username'
                      onChangeText={userName => this.setState({ userName })}
                      />
                  </View>
                  <View style={styles.forminput}>
                      <Text style={styles.formtext}>Password</Text>
                      <TextInput
                       style={styles.input}
                       placeholder='Masukkan Password'
                       onChangeText={password => this.setState({ password })}
                       secureTextEntry={true}
                      />
                  </View>
                  <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
                  <Button title='Login' onPress={() => this.loginHandler()} />
              </View>
          </ScrollView>
      </KeyboardAvoidingView>
    )
  }
};


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerView: {
        backgroundColor: '#fff',
        marginTop: 63,
        alignItems: 'center',
        flex: 1
    },
    logintext: {
        fontSize: 24,
        color: '#003366',
        textAlign: "center",
        marginVertical: 20
    },
    formtext: {
        color: '#003366',
    },
    atautext: {
        fontSize: 20,
        color: '#3EC6FF',
        textAlign: "center"
    },
    forminput: {
        marginHorizontal: 30,
        marginVertical: 5,
        alignContent: 'center',
        width: 294
    },
    input: {
        height: 40,
        borderColor: '#003366',
        borderWidth: 1
    },
    vbutton: {
        marginHorizontal: 90,
        borderRadius: 10,
        marginVertical: 10,
    },
    btlogin: {
        alignItems: "center",
        backgroundColor: "#3EC6FF",
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 30,
        marginBottom: 10,
        width: 140
    },
    btreg: {
        alignItems: "center",
        backgroundColor: "#003366",
        textDecorationColor: '#000',
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 30,
        marginTop: 10,
        width: 140
    },
    textbt: {
        color: 'white',
        textAlign: "center",
        fontSize: 15,
        fontWeight: "bold",
    },
    kotaklogin: {
        marginTop: 20,
        alignItems: 'center'
    },
    errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  }
});
